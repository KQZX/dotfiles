" Plugins
call plug#begin('~/.nvim/plugged')

Plug 'https://github.com/preservim/nerdtree'
Plug 'https://github.com/itchyny/lightline.vim'
Plug 'https://github.com/arcticicestudio/nord-vim'

call plug#end()

" General settings
set number
set noshowmode
syntax on
set termguicolors

" NERDTree
autocmd VimEnter * NERDTree | wincmd p
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
    \ quit | endif
autocmd BufWinEnter * silent NERDTreeMirror

" Lightline Theme
let g:lightline = {
      \ 'colorscheme': 'nord',
      \ }

" Colorscheme
colorscheme nord
